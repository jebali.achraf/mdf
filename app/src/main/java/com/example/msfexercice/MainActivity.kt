package com.example.msfexercice

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

const val EXTRA_ANGLE_VALUE = "degree_value"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val string = findViewById<EditText>(R.id.string)
        val angle = findViewById<EditText>(R.id.angle)
        val nextButton = findViewById<Button>(R.id.nextButton)
        angle.visibility = View.GONE
        string.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().length !== 0 && !p0.toString()[0].isUpperCase()) {
                    string.error = getString(R.string.string_error)
                    string.setText("")
                } else {
                    angle.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        angle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().length !== 0 && p0.toString().toFloat() > 360) {
                    angle.error = getString(R.string.angle_error)
                    nextButton.isEnabled = false
                } else
                    nextButton.isEnabled = true
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        nextButton.setOnClickListener {
            val intent = Intent(this,ImageActivity::class.java)
            intent.putExtra(EXTRA_ANGLE_VALUE, angle.text.toString().toFloat())
            startActivity(intent)
        }
    }
}
