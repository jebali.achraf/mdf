package com.example.msfexercice

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_image.*


class ImageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        val angle = intent.getFloatExtra(EXTRA_ANGLE_VALUE, 0f)

        val actionbar = supportActionBar
        actionbar!!.title = String.format(getString(R.string.image_activity_name), angle);
        actionbar.setDisplayHomeAsUpEnabled(true)

        image.rotation = angle

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}